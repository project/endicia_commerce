# README file for Endicia for Commerce

What does Endicia for Commerce do?
--------------------
This module integrates the label printing services provided by Grindflow Endicia
Cloud (GEC) to your Commerce site, allowing to you generate and print postage
labels from quickly and conveniently after receiving orders.

It requires the Grindflow Endicia Cloud module available for download here:
* http://drupal.org/project/endicia


Requirements
------------
Endicia for Commerce is an extension of the Grindflow Endicia Cloud module,
available here: http://drupal.org/project/endicia

Please keep in mind that while the Grindflow Endicia Cloud modules are free of
charge, a paid monthly subscription is required in order to print any labels.

It's easy! Sign up for an account today: http://grindflow.com/usps-grindflow-endicia-cloud-services


Installation
------------
For detailed instructions on how to install modules to your Drupal site, please
visit: http://drupal.org/documentation/install/modules-themes

1. Download & install Grindflow Endicia Cloud (see its README.txt for detailed
   instructions on how to do so).

2. Download & extract Endicia for Commerce to your site modules folder.

3. Visit "Administer > Site Building > Modules" and enable the Endicia for
   Commerce module.

4. Visit "Administer > Store > Configuration > Endicia Commerce" and configure
   the store address (used for automatically filling the label's origin address)
   as well as the shipping field names used to determine the customer address.

5. The 7.x version of Endicia for Commerce requires that each product have a
   weight (in oz) configured in a text field called 'field_weight'. To add a
   weight field in your default product type, navigate to the page at
   admin/commerce/products/types/product/fields. If you have configured other
   product types, you must configure the field on them too.


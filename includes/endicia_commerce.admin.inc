<?php
/**
 * @file
 * Form renderer callback functions for endicia_commerce module.
 */

/**
 * Page callback: endicia_commerce_order_ui_form().
 *
 * Display Endicia postage label in a tabular form.
 * @see endicia_commerce_order_ui_form_submit()
 */
function endicia_commerce_order_ui_form($form, &$form_state, $commerce_order) {
  $form = array();

  $header = array(
    array('data' => t('Date created'), 'field' => 'timestamp', 'sort' => 'desc'),
    array('data' => t('Tracking number')),
    array('data' => t('Actions')),
  );

  // Retrieving Endicia label details from the database.
  $query = db_select('endicia_commerce_shipping', 'ecs');
  $labels = $query
    ->fields('ecs')
    ->condition('ecs.order_id', $commerce_order->order_id)
    ->execute()
    ->fetchAll();

  $rows = array();
  if (empty($labels)) {
    $rows[] = array(array('data' => t('No labels have been printed yet.'), 'colspan' => count($header)));
  }
  else {
    foreach ($labels as $label) {
      $links = array();
      $file = NULL;
      if ($label->label_fid) {
        $file = file_load($label->label_fid);
        $links['print'] = array(
          'title' => t('Print'),
          'href' => 'endicia/print/' . file_uri_target($file->uri),
          'weight' => 0,
        );
      }
      $links['refund'] = array(
        'title' => t('Refund'),
        'href' => 'admin/endicia/account/refund/PICNumber/' . $label->tracking_number,
        'weight' => 1,
      );
      drupal_alter('endicia_label_archive_links', $links, $label, $file);
      uasort($links, 'drupal_sort_weight');

      $title = $label->tracking_number;

      // Create the row array and add it to the list of all rows.
      $row = array();
      $row[] = check_plain(format_date($label->ship_date));
      $row[] = check_plain($title);
      $row[] = theme('links', array('links' => $links));
      $rows[] = $row;
    }
  }

  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#prefix' => '<div class="endicia-table"><strong>' . t('Endicia Shipments') . '</strong>',
    '#suffix' => '</div>',
  );
  $form['new_shipment'] = array(
    '#type' => 'select',
    '#options' => endicia_commerce_get_preset_labels(),
    '#title' => t('New shipment'),
  );
  $form['order_id'] = array(
    '#type' => 'value',
    '#value' => $commerce_order,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#submit' => array('endicia_commerce_order_ui_form_submit'),
  );

  return $form;
}

/**
 * Endicia for commerce page submit handler.
 * @see endicia_commerce_order_ui_form()
 */
function endicia_commerce_order_ui_form_submit($form, &$form_state) {
  $commerce_order_id = $form_state['values']['order_id']->order_id;
  // Load order by order ID.
  $commerce_order = commerce_order_load($commerce_order_id);

  // Retrieve customer's profile shipping ID.
  $field_name = endicia_commerce_get_shipping_profile_field_name();
  $items = field_get_items('commerce_order', $commerce_order, $field_name);
  if (empty($items)) {
    drupal_set_message(t("Could not load the customer's shipping profile; please make sure a shipping address was entered on this order."), 'error');
    $form_state['rebuild'] = TRUE;
    return;
  }
  $shipping_profile_id = $items[0]['profile_id'];

  // Load customer's profile shipping addressfield.
  $shipping_profile = commerce_customer_profile_load($shipping_profile_id);

  if ($form_state['values']['new_shipment'] == 'manually') {
    // If using the drop-down from the Commerce order list, a ?destination=
    // parameter is added that overrides our goto here.
    unset($_GET['destination']);
    drupal_goto('admin/commerce/orders/' . $commerce_order_id . '/endicia/shipment/details');
  }
  else {
    // Get label presets.
    $presets = endicia_label_preset_get_all();
    if ($presets) {
      foreach ($presets as $preset) {
        if ($preset['preset_id'] == $form_state['values']['new_shipment']) {
          $mailpieceshape = $preset['mail_shape'];
          $mail_class_domestic = $preset['mail_class_domestic'];
          $mail_class_international = $preset['mail_class_international'];
          break;
        }
        else {
          $mailpieceshape = '';
        }
      }
    }

    $address = variable_get('endicia_commerce_store_address');
    if (!(empty($address['country'])) && !(empty($address['organisation_name']))
        && !(empty($address['thoroughfare'])) && !(empty($address['locality']))
        && !(empty($address['postal_code'])) && !(empty($address['administrative_area']))) {
      // Determine order's total weight.
      $total_weight = endicia_commerce_get_order_total_weight($commerce_order);
      $shipment_details = array();

      $items = field_get_items('commerce_customer_profile', $shipping_profile, 'commerce_customer_address');
      if (empty($items)) {
        drupal_set_message(t("Could not load the address on the customer's shipping profile; please make sure a shipping address was entered on this order."), 'error');
        $form_state['rebuild'] = TRUE;
        return;
      }
      $address = $items[0];

      $shipment_details['values']['d_country'] = $address['country'];
      $shipment_details['values']['order_id'] = $commerce_order_id;
      $shipment_details['values']['mailpiece'] = $mailpieceshape;
      $shipment_details['values']['weight'] = $total_weight;
      $shipment_details['values']['o_postal'] = isset($address['postal_code']) ? $address['postal_code'] : '';
      $shipment_details['values']['d_postal'] = $address['postal_code'];

      $postage_cost = endicia_commerce_calculatepostagerates($shipment_details);
      $_SESSION['postage_cost'] = $postage_cost;
      $_SESSION['mailpieceshape'] = $mailpieceshape;
      $_SESSION['mail_ship_class']['domestic'] = $mail_class_domestic;
      $_SESSION['mail_ship_class']['international'] = $mail_class_international;
      drupal_goto('admin/commerce/orders/' . $commerce_order_id . '/endicia/shipment/quotes');

    }
    else {
      drupal_set_message(t('The Endicia for Drupal Commerce store address is not configured. Please configured store address !store_address_link.', array('!store_address_link' => l(t('here'), 'admin/commerce/config/endicia_commerce'))), 'error');
      $form_state['rebuild'] = TRUE;
      return;
    }
  }
}


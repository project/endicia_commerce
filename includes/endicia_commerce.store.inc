<?php
/**
 * @file
 * Form renderer callback functions for endicia_commerce store information.
 */

/**
 * Endicia store information form.
 * @see endicia_commerce_store_form_submit()
 */
function endicia_commerce_store_form() {
  $form['#attributes'] = array('class' => array('endicia-commerce-store-form'));
  $address = variable_get('endicia_commerce_store_address');
  $form['store'] = array(
    '#type' => 'fieldset',
    '#title' => t('Store Information'),
    '#description' => t('This information will be used for automatically filling the origin address when requesting USPS labels.'),
  );
  $form['store']['endicia_commerce_store_address'] = array(
    '#type' => 'addressfield',
    '#required' => TRUE,
    '#title' => t('Store Address'),
    '#handlers' => array(
      'address' => 'address',
      'organisation' => 'organisation',
    ),
    '#default_value' => array(
      'organisation_name' => isset($address['organisation_name']) ? $address['organisation_name'] : '',
      'thoroughfare' => isset($address['thoroughfare']) ? $address['thoroughfare'] : '',
      'premise' => isset($address['premise']) ? $address['premise'] : '',
      'locality' => isset($address['locality']) ? $address['locality'] : '',
      'administrative_area' => isset($address['administrative_area']) ? $address['administrative_area'] : '',
      'postal_code' => isset($address['postal_code']) ? $address['postal_code'] : '',
      'country' => isset($address['country']) ? $address['country'] : 'US',
    ),
    '#description' => "Please enter the store address.",
    '#maxlength' => 60,
  );

  $form['store']['endicia_commerce_store_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#size' => 16,
    '#maxlength' => 32,
    '#required' => FALSE,
    '#default_value' => variable_get('endicia_commerce_store_phone'),
  );
  $form['store']['endicia_commerce_store_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#size' => 32,
    '#required' => FALSE,
    '#default_value' => variable_get('endicia_commerce_store_email'),
  );

  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field configuration'),
    '#description' => t("The following configuration directs Endicia on which Commerce fields read in order to automatically load the customer's shipping address from an order."),
  );

  $field_info = field_info_fields();
  $profile_fields = array();
  $address_fields = array();
  foreach ($field_info as $field_name => $field) {
    if ($field['type'] == 'addressfield') {
      $address_fields[$field_name] = $field_name;
    }
    else if ($field['type'] == 'commerce_customer_profile_reference') {
      $profile_fields[$field_name] = $field_name;
    }
  }
  $fields = array();
  $form['fields']['endicia_commerce_order_shipping_profile_field'] = array(
    '#type' => 'select',
    '#title' => t('Profile reference field name'),
    '#description' => t("The name of the profile reference field on Commerce orders that points to a customer profile with address information. You can find this on the <a href='!url'>Commerce Order settings page</a>.", array('!url' => url('admin/commerce/config/order/fields'))),
    '#required' => TRUE,
    '#options' => $profile_fields,
    '#default_value' => endicia_commerce_get_shipping_profile_field_name(),
  );

  $form['fields']['endicia_commerce_order_customer_address_field'] = array(
    '#type' => 'select',
    '#title' => t("Customer profile's address field name"),
    '#description' => t("The name of the address field that stores customer address information on the customer profile. Generally, you can find this on the <a href='!url'>shipping customer profile's fields page</a>.", array('!url' => url('admin/commerce/customer-profiles/types/shipping/fields'))),
    '#required' => TRUE,
    '#options' => $address_fields,
    '#default_value' => endicia_commerce_get_customer_profile_address_field_name(),
  );
  return system_settings_form($form);
}

/**
 * Validate handler for the store information form.
 */
function endicia_commerce_store_form_validate($form, &$form_state) {
  $digitsonly = preg_replace("/[^0-9]/", "", $form_state['values']['endicia_commerce_store_phone']);
  if (strlen($digitsonly) > 10) {
    form_error($form['store']['endicia_commerce_store_phone'], t('Phone numbers must not contain more than 10 digits.'));
  }
}


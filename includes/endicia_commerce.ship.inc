<?php
/**
 * @file
 * Form renderer callback functions for endicia_commerce module.
 */

/**
 * Page callback: endicia_commerce_order_ui_shipment().
 *
 * Endicia shipment detail form.
 * @see endicia_commerce_order_ui_form_submit()
 */
function endicia_commerce_order_ui_shipment($form, &$form_state, $commerce_order_id) {
  // Load order by order ID.
  $commerce_order = commerce_order_load($commerce_order_id);
  // Retrieve customer's profile shipping ID.
  $shipping_field_name = endicia_commerce_get_shipping_profile_field_name();
  $items = field_get_items('commerce_order', $commerce_order, $shipping_field_name);
  if (empty($items)) {
    drupal_set_message(t("Could not load the customer's shipping profile; please make sure a shipping address was entered on this order."), 'error');
    drupal_goto('admin/commerce/orders/' . $commerce_order_id);
  }
  $shipping_profile_id = $items[0]['profile_id'];

  // Load customer's profile shipping addressfield.
  $shipping_profile = commerce_customer_profile_load($shipping_profile_id);
  $address_field_name = endicia_commerce_get_customer_profile_address_field_name();
  $items = field_get_items('commerce_customer_profile', $shipping_profile, $address_field_name);
  $address = $items[0];

  $form = array();
  $form['destination_address'] = array(
    '#type' => 'addressfield',
    '#required' => TRUE,
    '#title' => t('Destination Address'),
    '#handlers' => array(
      'address' => 'address',
      'name-full' => 'name-full',
      'organisation' => 'organisation',
    ),
    '#default_value' => array(
      'first_name' => isset($address['first_name']) ? $address['first_name'] : '',
      'last_name' => isset($address['last_name']) ? $address['last_name'] : '',
      'organisation_name' => '',
      'thoroughfare' => isset($address['thoroughfare']) ? $address['thoroughfare'] : '',
      'premise' => isset($address['premise']) ? $address['premise'] : '',
      'locality' => isset($address['locality']) ? $address['locality'] : '',
      'administrative_area' => isset($address['administrative_area']) ? $address['administrative_area'] : '',
      'postal_code' => isset($address['postal_code']) ? $address['postal_code'] : '',
      'country' => isset($address['country']) ? $address['country'] : 'US',
    ),
    '#description' => "Please enter the store address.",
    '#maxlength' => 60,
  );

  $origin_address = variable_get('endicia_commerce_store_address');

  $form['store'] = array(
    '#type' => 'fieldset',
    '#title' => t('Origin Address'),
  );
  $form['store']['endicia_commerce_store_address'] = array(
    '#type' => 'addressfield',
    '#required' => TRUE,
    '#title' => '',
    '#handlers' => array(
      'address' => 'address',
      'organisation' => 'organisation',
    ),
    '#default_value' => array(
      'organisation_name' => isset($origin_address['organisation_name']) ? $origin_address['organisation_name'] : '',
      'thoroughfare' => isset($origin_address['thoroughfare']) ? $origin_address['thoroughfare'] : '',
      'premise' => isset($origin_address['premise']) ? $origin_address['premise'] : '',
      'locality' => isset($origin_address['locality']) ? $origin_address['locality'] : '',
      'administrative_area' => isset($origin_address['administrative_area']) ? $origin_address['administrative_area'] : '',
      'postal_code' => isset($origin_address['postal_code']) ? $origin_address['postal_code'] : '',
      'country' => isset($origin_address['country']) ? $origin_address['country'] : 'US',
    ),
    '#description' => "Please enter the store address.",
    '#maxlength' => 60,
  );

  $form['store']['endicia_commerce_store_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#size' => 16,
    '#maxlength' => 32,
    '#required' => FALSE,
    '#default_value' => variable_get('endicia_commerce_store_phone'),
  );
  $form['store']['endicia_commerce_store_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#size' => 32,
    '#required' => FALSE,
    '#default_value' => variable_get('endicia_commerce_store_email'),
  );

  // Product details.
  $form['product'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $order_wrapper = entity_metadata_wrapper('commerce_order', $commerce_order_id);
  $order_total = $order_wrapper->commerce_order_total->value();

  // Order's subtotal.
  $subtotal = commerce_price_component_total($order_total, 'base_price');
  $form['product']['declared_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Declared value'),
    '#default_value' => commerce_currency_format($subtotal['amount'], $subtotal['currency_code']),
    '#required' => TRUE,
  );

  $total_weight = endicia_commerce_get_order_total_weight($commerce_order);
  $form['product']['weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#field_suffix' => t('oz'),
    '#default_value' => $total_weight,
    '#size' => 10,
    '#maxlength' => 10,
    '#required' => TRUE,
  );
  $form['product']['mailpiece'] = array(
    '#type' => 'select',
    '#options' => _endicia_pkg_types(),
    '#title' => t('Mailpiece Shape'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#submit' => array('endicia_commerce_order_ui_shipment_submit'),
  );

  $form['order_id'] = array(
    '#type' => 'value',
    '#value' => $commerce_order_id,
  );

  return $form;
}

/**
 * Submit action handler for Endicia shipment details.
 *
 * @see endicia_commerce_order_ui_shipment()
 */
function endicia_commerce_order_ui_shipment_submit($form, &$form_state) {
  $shipment_details['values']['order_id'] = $form_state['values']['order_id'];
  $shipment_details['values']['d_country'] = $form_state['values']['destination_address']['country'];
  $shipment_details['values']['o_country'] = $form_state['values']['endicia_commerce_store_address']['country'];

  $shipment_details['values']['d_firstname'] = $form_state['values']['destination_address']['first_name'];
  $shipment_details['values']['d_lastname'] = $form_state['values']['destination_address']['last_name'];
  $shipment_details['values']['d_company'] = $form_state['values']['destination_address']['organisation_name'];
  $shipment_details['values']['d_address1'] = $form_state['values']['destination_address']['thoroughfare'];
  $shipment_details['values']['d_address2'] = $form_state['values']['destination_address']['premise'];
  $shipment_details['values']['d_city'] = $form_state['values']['destination_address']['locality'];
  $shipment_details['values']['d_state'] = $form_state['values']['destination_address']['administrative_area'];
  $shipment_details['values']['d_postal'] = $form_state['values']['destination_address']['postal_code'];
  $shipment_details['values']['d_email'] = '';

  if (!(empty($form_state['values']['endicia_commerce_store_address']['country'])) && !(empty($form_state['values']['endicia_commerce_store_address']['organisation_name']))
        && !(empty($form_state['values']['endicia_commerce_store_address']['thoroughfare'])) && !(empty($form_state['values']['endicia_commerce_store_address']['locality']))
        && !(empty($form_state['values']['endicia_commerce_store_address']['postal_code'])) && !(empty($form_state['values']['endicia_commerce_store_address']['administrative_area']))) {
    $shipment_details['values']['o_company'] = $form_state['values']['endicia_commerce_store_address']['organisation_name'];
    $shipment_details['values']['o_address1'] = $form_state['values']['endicia_commerce_store_address']['thoroughfare'];
    $shipment_details['values']['o_address2'] = $form_state['values']['endicia_commerce_store_address']['premise'];
    $shipment_details['values']['o_city'] = $form_state['values']['endicia_commerce_store_address']['locality'];
    $shipment_details['values']['o_state'] = $form_state['values']['endicia_commerce_store_address']['administrative_area'];
    $shipment_details['values']['o_postal'] = $form_state['values']['endicia_commerce_store_address']['postal_code'];
    $shipment_details['values']['o_email'] = $form_state['values']['endicia_commerce_store_email'];
    $shipment_details['values']['o_phone'] = $form_state['values']['endicia_commerce_store_phone'];
  }
  else {
    drupal_set_message(t('The Endicia Commerce store address is not configured. Please configured store address !store_address_link.', array('!store_address_link' => l(t('here'), 'admin/commerce/config/endicia_commerce'))), 'error');
    drupal_goto('admin/commerce/orders/' . $form_state['values']['order_id'] . '/endicia/shipment/details');
  }

  $shipment_details['values']['mailpiece'] = $form_state['values']['mailpiece'];
  $shipment_details['values']['weight'] = $form_state['values']['weight'];

  $postage_cost = endicia_commerce_calculatepostagerates($shipment_details);
  $_SESSION['shipment_details'] = $shipment_details;
  $_SESSION['postage_cost'] = $postage_cost;

  drupal_goto('admin/commerce/orders/' . $form_state['values']['order_id'] . '/endicia/shipment/quotes');

}

/**
 * Endicia shipment quotes form.
 * @see endicia_commerce_order_ui_quotes_submit()
 */
function endicia_commerce_order_ui_shipment_quotes($form, &$form_state, $commerce_order_id) {
  // To extract shipping class field data from endicia module.
  if (module_exists('endicia') && function_exists('_endicia_mail_classes')) {
    $endicia_ship_classes = _endicia_mail_classes();
  }
  if (isset($_SESSION['postage_cost']) && !empty($_SESSION['postage_cost'])) {
    $ship_class_options = array();
    // Prepare shipping class options.
    foreach ($_SESSION['postage_cost'] as $key => $ship_class_cost) {
      if (isset($endicia_ship_classes[$key])) {
        $ship_class_options[$key] = $endicia_ship_classes[$key] . " - $" . number_format((float) $ship_class_cost, 2, '.', '');
      }
      else {
        $ship_class_options[$key] = $key . " - $" . number_format((float) $ship_class_cost, 2, '.', '');
      }
    }

    // Determine default shipping class.
    foreach ($_SESSION['postage_cost'] as $key => $value) {
      if (isset($_SESSION['mail_ship_class'])) {
        if ($key == $_SESSION['mail_ship_class']['domestic']) {
          $default_mail_class = $key;
          break;
        }
        elseif ($key == $_SESSION['mail_ship_class']['international']) {
          $default_mail_class = $key;
          break;
        }
      }
    }
  }
  else {
    drupal_set_message(t("Please select label preset"), 'error');
    drupal_goto('admin/commerce/orders/' . $commerce_order_id . '/endicia');
  }

  $account_info = endicia_get_account_status();

  $form = array();
  $form['intro'] = array(
    '#type' => 'markup',
    '#markup' => t('Select a shipping class and click on Confirm Shipment to complete the shipment. Your account balance will be deducted the amount indicated next to the shipping class.'),
  );
  $form['label_container']['endicia_ship_class'] = array(
    '#type' => 'select',
    '#title' => t('Shipping Class:'),
    '#options' => $ship_class_options,
    '#default_value' => (isset($default_mail_class)) ? $default_mail_class : '',
    '#multiple' => FALSE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#submit' => array('endicia_commerce_order_ui_quotes_submit'),
  );
  $form['order_id'] = array(
    '#type' => 'value',
    '#value' => $commerce_order_id,
  );
  $fund_req_url = '/admin/endicia/account/purchase';
  $form['endicia_balance'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="fund-req">',
    '#markup' => t('Your account balance is') . ' <strong>$' . $account_info['PostageBalance'] .
    '</strong><br/>To add funds, ' .
    l(t('click here'), $fund_req_url, array('attributes' => array('target' => '_blank'))),
    '#suffix' => '</div>',
  );

  return $form;
}

/**
 * Shipping quotes submit handler.
 *
 * @see endicia_commerce_order_ui_quotes()
 */
function endicia_commerce_order_ui_quotes_submit($form, &$form_state) {
  $commerce_order_id = $form_state['values']['order_id'];

  if (isset($_SESSION['shipment_details'])) {
    // For manual label generation.
    $shipment_details = $_SESSION['shipment_details'];
  }
  else {
    // For label generation using Endicia label preset.

    // Load order by order ID.
    $commerce_order = commerce_order_load($commerce_order_id);
    // Determine order's total weight.
    $total_weight = endicia_commerce_get_order_total_weight($commerce_order);
    // Retrieve customer's profile shipping ID.
    $shipping_field_name = endicia_commerce_get_shipping_profile_field_name();
    $items = field_get_items('commerce_order', $commerce_order, $shipping_field_name);
    if (empty($items)) {
      drupal_set_message(t("Could not load the customer's shipping profile; please make sure a shipping address was entered on this order."), 'error');
      drupal_goto('admin/commerce/orders/' . $commerce_order_id);
    }
    $shipping_profile_id = $items[0]['profile_id'];

    // Load customer's profile shipping addressfield.
    $shipping_profile = commerce_customer_profile_load($shipping_profile_id);
    $address_field_name = endicia_commerce_get_customer_profile_address_field_name();
    $items = field_get_items('commerce_customer_profile', $shipping_profile, $address_field_name);
    $address = $items[0];

    $origin_address = variable_get('endicia_commerce_store_address');

    $shipment_details['values']['order_id'] = $commerce_order_id;
    $shipment_details['values']['d_country'] = $address['country'];
    $shipment_details['values']['o_country'] = isset($origin_address['country']) ? $origin_address['country'] : '';

    $shipment_details['values']['d_firstname'] = $address['first_name'];
    $shipment_details['values']['d_lastname'] = $address['last_name'];
    $shipment_details['values']['d_company'] = $address['organisation_name'];
    $shipment_details['values']['d_address1'] = $address['thoroughfare'];
    $shipment_details['values']['d_address2'] = $address['premise'];
    $shipment_details['values']['d_city'] = $address['locality'];
    $shipment_details['values']['d_state'] = $address['administrative_area'];
    $shipment_details['values']['d_postal'] = $address['postal_code'];
    $shipment_details['values']['d_email'] = '';

    $shipment_details['values']['o_company'] = isset($origin_address['organisation_name']) ? $origin_address['organisation_name'] : '';
    $shipment_details['values']['o_address1'] = isset($origin_address['thoroughfare']) ? $origin_address['thoroughfare'] : '';
    $shipment_details['values']['o_address2'] = isset($origin_address['premise']) ? $origin_address['premise'] : '';
    $shipment_details['values']['o_city'] = isset($origin_address['locality']) ? $origin_address['locality'] : '';
    $shipment_details['values']['o_state'] = isset($origin_address['administrative_area']) ? $origin_address['administrative_area'] : '';
    $shipment_details['values']['o_postal'] = isset($origin_address['postal_code']) ? $origin_address['postal_code'] : '';
    $shipment_details['values']['o_phone'] = variable_get('endicia_commerce_store_phone');
    $shipment_details['values']['o_email'] = variable_get('endicia_commerce_store_email');
    $shipment_details['values']['weight'] = $total_weight;
  }
  // To generate Endicia postage label.
  endicia_commerce_generatelabel($shipment_details, $form_state);

}

